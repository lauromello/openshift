<h1 align="center"> Minishift </h1>

<p align="justify"> O que venha ser minishift - É uma ferramenta opensourse desenvolvida pela Red Hat. Que auxilia o processo de orquestração de containers onde sua plataforma tem o Kubernets customizado, com uma interfase amigável, onde podemos controlar todo o ciclo de vida da aplicação. Com uma integração facilitada com vários outras ferramentas e SDK’s para diferentes linguagens, tornando uma ferramenta completa para controlar os containers. 
O minishift é uma versão pequena do Openshift, que podemos realizar testes executando as aplicações seja do nosso repositório local ou de um remoto como por exemplo (GITLAB, GITHUB, etc.). Porém quero salientar que esta versão não possui suporte. </p>

<img src="https://img.shields.io/static/v1?label=Maven&message=framework&color=blue&style=for-the-badge&logo=MAVEN"/>
<img src="https://img.shields.io/static/v1?label=Docker&message=DevOps&color=blue&style=for-the-badge&logo=DOCKER"/>
<img src="https://img.shields.io/static/v1?label=Openshift&message=OKD&color=blue&style=for-the-badge&logo=OPENSHIFT"/>
<img src="https://img.shields.io/static/v1?label=virtualbox&message=Deploy&color=blue&style=for-the-badge&logo=VIRTUALBOX"/>
<img src="https://img.shields.io/static/v1?label=status&message=Concluido&color=green&style=for-the-badge&logo=Concluido"/>

## Implantações a serem realizadas para este projeto :
    (Neste projeto foi utilizado sistema Ubuntu 20.04.4 LTS)

:trophy: Instalção e configuração do GIT 

:trophy: Criação de um repositório local o remoto (GITHUB,GITLAB,etc) 

:trophy: configuração e instalação Docker

:trophy: configuração e instalação virtualbox

:trophy: configuração e instalação Minishift

> Status do Projeto: Concluido :heavy_check_mark:


### Soluções de alguns problemas

Veja alguns problemas que surgiram no desenvolvimento deste projeto e como os resolvi:
" Too many retries waiting for SSH to be available"
--> Verificar o status do minishift = minishift status
--> Remover o cache -> minishift delete --force --clear-cache
--> Caso o minishift ainda esteja em runnig --> minishift stop
--> restartar -> sudo service libvirtd restart
--> startar o minishift -> minishift start
