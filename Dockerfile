FROM openjdk:8u121-jre-alpine
ADD target/DemoApplication.java DemoApplication.java
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar /DemoApplication.java"]